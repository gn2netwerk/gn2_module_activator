## gn2 Module Activator
(c) 2015 [gn2netwerk](http://www.gn2-netwerk.de/), Coburg, Germany

### Installation

You have to add 2 Template blocks to "application/views/admin/tpl/module_list.tpl", see
"changed_full/module_list.tpl" for reference!

Then simply activate the module in the backend and use the added checkboxes and the new "Update" Button
to de-/activate multiple modules. You can also toggle all checkboxes at once.

![gn2_module_activator.png](https://bitbucket.org/repo/rERKpE/images/1429124358-gn2_module_activator.png)