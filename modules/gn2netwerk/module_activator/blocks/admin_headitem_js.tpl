[{$smarty.block.parent}]
[{if $oViewConf->getActiveClassName() eq "module_list"}]
    <script>
        function toggleModules(elem)
        {
            var checkboxes = document['search'].getElementsByTagName('input');

            for (var i=0; i<checkboxes.length; i++)  {
                if (checkboxes[i].type == 'checkbox')   {
                    checkboxes[i].checked = elem.checked;
                }
            }
        }
    </script>
[{/if}]
