[{$smarty.block.parent}]
<tr>
    <td colspan="2" align="left">
        <input type="checkbox" id="cb_modules_master" name="cb_modules_master" onclick="toggleModules(this)">Toggle
        <input type="submit" class="edittext" name="save" value="Update" onClick="Javascript:document.search.fnc.value='changeModuleStatus';" [{ $readonly }]>&nbsp;&nbsp;&nbsp;
    </td>
</tr>
<tr>
    <td colspan="2" align="left">
        [{foreach from=$oView->getMessagesActivated() item=message}]
            <div style='color:green;'>Activated module [{$message}]</div>
        [{/foreach}]
        [{foreach from=$oView->getMessagesDeactivated() item=message}]
            <div style='color:red;'>De-Activated module [{$message}]</div>
        [{/foreach}]
    </td>
</tr>
