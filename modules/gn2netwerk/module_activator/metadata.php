<?php
/**
 * OXID_Module_Activator
 *
 * PHP version 5
 *
 * @link     http://www.gn2-netwerk.de
 */

$aModule = array(
    'id'          => 'gn2_module_activator',
    'title'       => 'gn2 :: Module Activator',
    'description' => 'De-/aktiviert mehrere Module auf einmal.',
    'thumbnail'   => 'gn2.jpg',
    'version'     => '1.1.0',
    'author'      => 'gn2 netwerk',
    'extend'      => array(
        'module_list'   => 'gn2netwerk/module_activator/application/controllers/admin/module_activator_module_list',
    ),
    'files'       => array(
    ),
    'templates'       => array(
    ),
    'blocks'    => array(
        array(
            'template' => 'headitem.tpl',
            'block' => 'admin_headitem_js',
            'file' => '/blocks/admin_headitem_js.tpl'
        ),
        array(
            'template' => 'module_list.tpl',
            'block' => 'gn2_module_activator_list_checkbox',
            'file' => '/blocks/gn2_module_activator_list_checkbox.tpl'
        ),
        array(
            'template' => 'module_list.tpl',
            'block' => 'gn2_module_activator_list_submit',
            'file' => '/blocks/gn2_module_activator_list_submit.tpl'
        ),
    ),
    'events' => array(
        //'onActivate' => 'module_activator_setup::onActivate',
    ),
    'settings' => array(
    )
);