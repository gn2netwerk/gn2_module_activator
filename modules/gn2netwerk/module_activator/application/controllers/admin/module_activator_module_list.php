<?php
/**
 * GN2_ModuleActivator
 * @copyright GN2 netwerk
 * @link http://www.gn2-netwerk.de/
 * @author Stefan Moises, <sm@gn2.de>
 * @package admin
 * @version   1.1.0
 */
class module_activator_module_list extends module_activator_module_list_parent
{
    /**
     * Collect activated module ids
     * @var array
     */
    protected $_messagesActivated = array();

    /**
     * Collect deactivated module ids
     * @var array
     */
    protected $_messagesDeActivated = array();

    /**
     * Change module status to in-/active
     */
    public function changeModuleStatus()
    {
        $aAllModuleIds = oxRegistry::getConfig()->getRequestParameter('cb_modules');
        $aEnabledModules = array();
        $aDisabledModules = array();
        foreach ($aAllModuleIds as $moduleId => $status) {
            if ($status) {
                $aEnabledModules[] = $moduleId;
            } else {
                $aDisabledModules[] = $moduleId;
            }
        }
        $this->activateModules($aEnabledModules);
        $this->deactivateModules($aDisabledModules);
        // clear tmp files
        $this->clearTmp();
    }

    /**
     * Activate all checked modules
     * @param array $aModuleIds The module ids
     */
    public function activateModules($aModuleIds)
    {
        foreach ($aModuleIds as $sModule) {
            /** @var oxModule $oModule */
            $oModule = oxNew('oxModule');
            if (!$oModule->load($sModule)) {
                oxRegistry::get("oxUtilsView")->addErrorToDisplay(new oxException('EXCEPTION_MODULE_NOT_LOADED'));
                continue;
            }
            if ($oModule->isActive()) {
                continue;
            }
            try {
                if (class_exists('oxModuleInstaller')) {
                    /** @var oxModuleCache $oModuleCache */
                    $oModuleCache = oxNew('oxModuleCache', $oModule);
                    /** @var oxModuleInstaller $oModuleInstaller */
                    $oModuleInstaller = oxNew('oxModuleInstaller', $oModuleCache);

                    if ($oModuleInstaller->activate($oModule)) {
                        $this->_aViewData["updatenav"] = "1";
                        $this->_messagesActivated[] = $sModule;
                    }
                } else {
                    if ($oModule->activate()) {
                        $this->_aViewData["updatenav"] = "1";
                        $this->_messagesActivated[] = $sModule;
                    }
                }

            } catch (oxException $oEx) {
                $this->_messagesActivated[] = $sModule . "<br/>Error: (" . $oEx->getMessage() . ")";
            }
        }
    }
    /**
     * Dectivate all NOT checked modules
     * @param array $aModuleIds The module ids
     */
    public function deactivateModules($aModuleIds)
    {
        foreach ($aModuleIds as $sModule) {
            /** @var oxModule $oModule */
            $oModule = oxNew('oxModule');
            if (!$oModule->load($sModule)) {
                oxRegistry::get("oxUtilsView")->addErrorToDisplay(new oxException('EXCEPTION_MODULE_NOT_LOADED'));
                continue;
            }
            if (!$oModule->isActive()) {
                continue;
            }
            try {
                if (class_exists('oxModuleInstaller')) {
                    /** @var oxModuleCache $oModuleCache */
                    $oModuleCache = oxNew('oxModuleCache', $oModule);
                    /** @var oxModuleInstaller $oModuleInstaller */
                    $oModuleInstaller = oxNew('oxModuleInstaller', $oModuleCache);

                    if ($oModuleInstaller->deactivate($oModule)) {
                        $this->_aViewData["updatenav"] = "1";
                        $this->_messagesDeActivated[] = $sModule;
                    }
                } else {
                    if ($oModule->deactivate()) {
                        $this->_aViewData["updatenav"] = "1";
                        $this->_messagesDeActivated[] = $sModule;
                    }
                }
            } catch (oxException $oEx) {
                $this->_messagesDeActivated[] = $sModule . "<br/>Error: (" . $oEx->getMessage() . ")";
            }
        }
    }

    /**
     * Get info about deactivated modules
     * @return array
     */
    public function getMessagesActivated()
    {
        return  $this->_messagesActivated;
    }

    /**
     * Get info about deactivated modules
     * @return array
     */
    public function getMessagesDeactivated()
    {
        return  $this->_messagesDeActivated;
    }

    /**
     * Clear tmp dir
     */
    public function clearTmp()
    {
        // delete tmp
        $tmpdir = oxRegistry::getConfig()->getConfigParam('sCompileDir');
        $d = opendir($tmpdir);
        while (($filename = readdir($d) ) !== false) {
            $filepath = $tmpdir . $filename;
            if (is_file($filepath)) {
                unlink($filepath);
            }
        }
    }
}